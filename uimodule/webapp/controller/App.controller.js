sap.ui.define(["ventia/polaris/etimesheethours/controller/BaseController"], function (Controller) {
    "use strict";

    return Controller.extend("ventia.polaris.etimesheethours.controller.App", {
     
      onInit: function () {
      this.oRouter = this.getOwnerComponent().getRouter();
      this.oRouter.attachRouteMatched(this.onRouteMatched, this);
      //this.oRouter.attachBeforeRouteMatched(this.onBeforeRouteMatched, this);
      this.oRouter.navTo("master");
    },

    onRouteMatched: function(oEvent) {

    },

    });
});
