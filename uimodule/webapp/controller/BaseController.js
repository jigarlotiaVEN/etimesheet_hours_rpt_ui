sap.ui.define(
  [
    "sap/ui/core/mvc/Controller",
    "sap/ui/core/routing/History",
    "sap/ui/core/UIComponent",
    "ventia/polaris/etimesheethours/model/formatter",
    "ventia/polaris/etimesheethours/util/ServiceUtil",
    "ventia/polaris/etimesheethours/util/Config",
    "ventia/polaris/etimesheethours/util/Filter",
    "sap/m/MessageBox",
    "sap/m/MessageToast",
    "sap/ui/core/Fragment",
    "sap/ui/Device",
    "ventia/polaris/etimesheethours/model/default",
  ],
  function (Controller, History, UIComponent, formatter, ServiceUtil, Config, Filter, MessageBox, MessageToast, Fragment, Device, Default) {
    "use strict";

    return Controller.extend("ventia.polaris.etimesheethours.controller.BaseController", {
      formatter: formatter,

      /**
       * Convenience method for getting the view model by name in every controller of the application.
       * @public
       * @param {string} sName the model name
       * @returns {sap.ui.model.Model} the model instance
       */
      getModel: function (sName) {
        return this.getView().getModel(sName);
      },

      /**
       * Convenience method for setting the view model in every controller of the application.
       * @public
       * @param {sap.ui.model.Model} oModel the model instance
       * @param {string} sName the model name
       * @returns {sap.ui.mvc.View} the view instance
       */
      setModel: function (oModel, sName) {
        return this.getView().setModel(oModel, sName);
      },

      /**
       * Convenience method for getting the resource bundle.
       * @public
       * @returns {sap.ui.model.resource.ResourceModel} the resourceModel of the component
       */
      getResourceBundle: function () {
        return this.getOwnerComponent().getModel("i18n").getResourceBundle();
      },

      /**
       * Method for navigation to specific view
       * @public
       * @param {string} psTarget Parameter containing the string for the target navigation
       * @param {Object.<string, string>} pmParameters? Parameters for navigation
       * @param {boolean} pbReplace? Defines if the hash should be replaced (no browser history entry) or set (browser history entry)
       */
      navTo: function (psTarget, pmParameters, pbReplace) {
        this.getRouter().navTo(psTarget, pmParameters, pbReplace);
      },

      getRouter: function () {
        return UIComponent.getRouterFor(this);
      },

      fnGetTokenModel: function () {
        return this.getOwnerComponent().getModel("Token");
      },
      fnGetDataModel: function () {
        return this.getOwnerComponent().getModel("local");
      },
      fnServiceUtil: function () {
        return ServiceUtil;
      },
      fnConfig: function () {
        return Config;
      },

      fnFilter: function () {
        return Filter;
      },

      //Retrieve Time Data
      fnReadTimeData: function () {

        var oParams = this.fnsetDate();
        //make view visible
        this.getView().setBusy(true);
        var toRetrieveTimesheetData = this.fnServiceUtil().fnAjaxQuery(this.fnConfig().getTimesheetData(), "GET", oParams);
        toRetrieveTimesheetData.then(function (data) {
          //data = Default.getChartData();
          var treeData = this.fnBuildTree(data);
          this.fnGetDataModel().setProperty("/HoursData", treeData);
          this.getView().setBusy(false);
        }.bind(this)).catch(function (oError) {
          // // console.log(e.message);
          this.getView().setBusy(false);
          try {
            MessageBox.error(oError.status + " ERROR: " + oError.responseJSON.message);
          } catch (error) {
            MessageBox.error(oError.statusText);
          }
        }.bind(this));
      },

      fnBuildTree: function (Data) {
        var returnData = {};
        var categoriesMain = [];
        var categoriesTop = {};
        var categoriesItem = {};

        Data.forEach(function (m) {
          m.weekperiods.forEach(function (o) {
            categoriesTop = {};
            categoriesTop.exp = "";
            categoriesTop.PERNR = m.PERNR;
            categoriesTop.empName = m.empName;
            categoriesTop.PayrollBegda = "";
            categoriesTop.PayrollEndda = "";
            categoriesTop.WeekBegda = "";
            categoriesTop.WeekEndda = "";
            categoriesTop.WeeklyPlannedHours = "";
            categoriesTop.Day = "";
            categoriesTop.Date = "";
            categoriesTop.Manager = "";
            categoriesTop.PlannedHours = o.WeeklyPlannedHours;
            categoriesTop.TimeSliceCount = o.WeeklyTimeSliceCount;
            categoriesTop.SubmittedHours = o.WeeklySubmittedHours;
            categoriesTop.SavedAsDraftCount = o.WeeklySavedAsDraftCount;
            categoriesTop.RejectedCount = o.WeeklyRejectedCount;
            categoriesTop.AwaitingApprovalCount = o.WeeklyAwaitingApprovalCount;
            categoriesTop.ApprovedCount = o.WeeklyApprovedCount;
            categoriesTop.UnsubmittedCount = o.WeeklyUnsubmittedCount;
            categoriesTop.Variance = o.WeeklyVariance;
            categoriesTop.categories = [];
            o.dayperiod.forEach(function (p) {
              categoriesItem = {};
              categoriesItem.exp = "";
              categoriesItem.PERNR = m.PERNR;
              categoriesItem.empName = "__" + m.empName;
              categoriesItem.PayrollBegda = p.PayrollBegda;
              categoriesItem.PayrollEndda = p.PayrollEndda;
              categoriesItem.WeekBegda = p.WeekBegda;
              categoriesItem.WeekEndda = p.WeekEndda;
              categoriesItem.WeeklyPlannedHours = p.WeeklyPlannedHours;
              categoriesItem.Day = p.Day;
              categoriesItem.Date = p.StartDate;
              categoriesItem.Manager = "";
              categoriesItem.PlannedHours = p.PlannedHours;
              categoriesItem.TimeSliceCount = p.dailyTotalTimeSliceCount;
              categoriesItem.SubmittedHours = p.dailyTotalSubmittedHours;
              categoriesItem.SavedAsDraftCount = p.dailyTotalSavedAsDraftCount;
              categoriesItem.RejectedCount = p.dailyTotalRejectedCount;
              categoriesItem.AwaitingApprovalCount = p.dailyTotalAwaitingApprovalCount;
              categoriesItem.ApprovedCount = p.dailyTotalApprovedCount;
              categoriesItem.UnsubmittedCount = p.unsubmittedCount;
              categoriesTop.Variance = o.dailyVariance;
              categoriesTop.categories.push(categoriesItem);
            });
            categoriesMain.push(categoriesTop);
          });
        });
        return categoriesMain;
      },

      fnsetDate: function (oEvent) {
        var date = new Date();
        var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
        var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
        var oParams = {};
        oParams.begda = formatter.dateToYYYYMMDD(firstDay);
        oParams.endda = formatter.dateToYYYYMMDD(lastDay);

        var Dates = {};
        Dates.begda = firstDay;
        Dates.endda = lastDay;
        //set date model for date range
        this.fnGetDataModel().setProperty('/Dates', Dates);

        return oParams;
      },

      getViewSettingsDialog: function (sDialogFragmentName) {
        var pDialog = this._mViewSettingsDialogs[sDialogFragmentName];

        if (!pDialog) {
          pDialog = Fragment.load({
            id: this.getView().getId(),
            name: sDialogFragmentName,
            controller: this
          }).then(function (oDialog) {
            if (Device.system.desktop) {
              oDialog.addStyleClass("sapUiSizeCompact");
            }
            return oDialog;
          });
          this._mViewSettingsDialogs[sDialogFragmentName] = pDialog;
        }
        return pDialog;
      },

      loadDialog: function (sFragment) {

        if (!this.pdialog) {
          Fragment.load({
            id: this.getView().getId(),
            name: sFragment,
            controller: this
          }).then(function (oValueHelpDialog) {
            this.pdialog = oValueHelpDialog;
            this.getView().addDependent(this.pdialog);
            this.pdialog.open();
          }.bind(this));
        }
        else {
          this.pdialog.open();
        }

      },

      onNavBack: function () {
        var sPreviousHash = History.getInstance().getPreviousHash();

        if (sPreviousHash !== undefined) {
          window.history.back();
        } else {
          this.getRouter().navTo("appHome", {}, true /*no history*/);
        }
      }
    });
  }
);
