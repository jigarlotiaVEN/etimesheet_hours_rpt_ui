sap.ui.define([
  "ventia/polaris/etimesheethours/controller/BaseController",
  "sap/ui/model/FilterOperator",
  "sap/ui/model/Filter",
  "ventia/polaris/etimesheethours/model/formatter",
  "ventia/polaris/etimesheethours/model/models",
  "ventia/polaris/etimesheethours/model/default",
  "sap/ui/core/Fragment",
  "sap/ui/model/Sorter",
  "sap/ui/core/util/Export",
  "sap/ui/core/util/ExportTypeCSV",
  "sap/m/MessageBox",
  "sap/ui/export/Spreadsheet",
], function (Controller, FilterOperator, Filter, formatter, Models, Default, Fragment, Sorter, Export, ExportTypeCSV, MessageBox, Spreadsheet, ) {
  "use strict";
  return Controller.extend("ventia.polaris.etimesheethours.controller.Master", {
    formatter: formatter,
    onInit: function () {
      var setupData = Default.fnsetupData();
      this.fnGetDataModel().setProperty("/Status", setupData.Status);
      this.fnGetDataModel().setProperty("/PayCode", setupData.PayCode);
      this.fnReadTimeData();   
      this._mViewSettingsDialogs = {};
    },

    onPersoButtonPressed: function () {
      var columnData = Models.getColumns();
      this.fnGetDataModel().setProperty("/ColumnsItems", columnData.ColumnsItems);
      this.fnGetDataModel().setProperty("/Items", columnData.Items);
      this.fnGetDataModel().setProperty("/ShowResetEnabled", columnData.ShowResetEnabled);
      var sDialog = "ventia.polaris.etimesheethours.view.Fragments.PersonDialog";
      this.loadDialog(sDialog);
    },


    onFetchReport: function (oEvent) {
      this.filterReport();
    },

    filterReport: function () {
      var aFilter = [];
      var aPernr = this.fnGetDataModel().getProperty("/PERNR");
      if (aPernr) {
        aFilter.push(new Filter("PERNR", FilterOperator.Contains, aPernr));
      }
 
      //var afilterDates = this.fnGetDataModel().getProperty("/Dates");
      //var x = formatter.dateToYYYYMMDD(afilterDates.begda)
      //if (afilterDates) {
      //  aFilter.push(new Filter("DATE", FilterOperator.BT, formatter.dateToYYYYMMDD(afilterDates.begda), formatter.dateToYYYYMMDD(afilterDates.endda)));
      //}      
  

      var oList = this.byId("HourTreeTable");
      var oBinding = oList.getBinding("rows");
      oBinding.filter(aFilter);

    },

    handleFilterDateChange: function (oEvent) {
      //this.filterReport();
      var aDates = this.fnGetDataModel().getProperty("/Dates");
      this.fnGetDataModel().setProperty("/filterDates", aDates);
      this.filterReport();
    },

    onSearchPernr: function (event) {
      this.filterReport();
    },
    onSearchPayrollArea: function (event) {
      this.filterReport();
    },    
        

    onStatusChange: function (oEvent) {
      var aSelectedData = oEvent.getParameters().selectedItems;
      this.fnGetDataModel().setProperty("/filterStatus", aSelectedData);
      this.filterReport();
    },

    onSearch: function (oEvent) {
      var aFilters = [];
      var sQuery = oEvent.getSource().getValue();

      if (sQuery && sQuery.length > 0) {
        var fullName = new Filter("empl_FullName", FilterOperator.Contains, sQuery);

        var Pernr = new Filter("PERNR", FilterOperator.Contains, sQuery);

        var LineManager = new Filter("line_Mgr_FullName", FilterOperator.Contains, sQuery);

        var DelegateManager = new Filter("del_Mgr_FullName", FilterOperator.Contains, sQuery);

        var EscalationManager = new Filter("escalation_Mgr_FullName", FilterOperator.Contains, sQuery);

        var TimeIn = new Filter("TIMEIN", FilterOperator.Contains, sQuery);

        var Timeout = new Filter("TIMEOUT", FilterOperator.Contains, sQuery);

        var oDate = new Filter("TIMESHEET_DATE", FilterOperator.EQ, formatter.dateToYYYYMMDDSearch(sQuery));

        aFilters = new Filter([fullName, Pernr, LineManager, DelegateManager, EscalationManager, TimeIn, Timeout, oDate], sQuery);
      }

      // update list binding
      var oList = this.byId("idTableData");
      var oBinding = oList.getBinding("items");
      oBinding.filter(aFilters);

    },

    onPayCodeChange: function (oEvent) {
      var oValue = oEvent.getParameters().value;
      this.fnGetDataModel().setProperty("/filterPayCode", oValue);
      this.filterReport();
    },

    onAllowanceClose: function (oEvent) {
      this.pdialog.close();
    },

    onSeeAllowances: function (oEvent) {
      //var oTable = this.getView().byId("idTableData");
      var oSelectedContext = oEvent.getSource().getBindingContext("local");
      var aData = this.fnGetDataModel().getProperty(oSelectedContext.sPath + "/allowanceItems");
      this.fnGetDataModel().setProperty("/allowances", aData);
      var sDialog = "ventia.polaris.etimesheethours.view.Fragments.AllowanceDialog";
      this.loadDialog(sDialog);

    },

    onSeeComments: function (oEvent) {

      var oTable = this.getView().byId("idTableData");
      var oSelectedContext = oEvent.getSource().getBindingContext("local");
      var aData = this.fnGetDataModel().getProperty(oSelectedContext.sPath + "/commentsItems");
      this.fnGetDataModel().setProperty("/comments", aData);


      var sDialog = "ventia.polaris.etimesheethours.view.Fragments.CommentsDialog";
      if (!this.commentsdialog) {
        Fragment.load({
          id: "CommentsDialog",
          name: sDialog,
          controller: this
        }).then(function (oValueHelpDialog) {
          this.commentsdialog = oValueHelpDialog;
          this.getView().addDependent(this.commentsdialog);
          this.commentsdialog.open();
        }.bind(this));
      }
      else {
        this.commentsdialog.open();
      }
    },

    onCommentDialogClose: function (oEvent) {
      this.commentsdialog.close();
    },
    onCommentsAfterClose: function () {
      this.commentsdialog.destroy();
      this.commentsdialog = null;
    },

    fnFilterTable: function (sQuery) {


    },

    handleSortButtonPressed: function () {
      var sDialog = "ventia.polaris.etimesheethours.view.Fragments.SortDialog";
      this.getViewSettingsDialog(sDialog)
        .then(function (oViewSettingsDialog) {
          oViewSettingsDialog.open();
        });
    },

    handleSortDialogConfirm: function (oEvent) {
      var oTable = this.byId("idTableData"),
        mParams = oEvent.getParameters(),
        oBinding = oTable.getBinding("items"),
        sPath,
        bDescending,
        aSorters = [];

      sPath = mParams.sortItem.getKey();
      bDescending = mParams.sortDescending;
      aSorters.push(new Sorter(sPath, bDescending));

      // apply the selected sort and group settings
      oBinding.sort(aSorters);
    },

    handleFilterButtonPressed: function () {
      var sDialog = "ventia.polaris.etimesheethours.view.Fragments.FilterDialog";
      this.getViewSettingsDialog(sDialog)
        .then(function (oViewSettingsDialog) {
          oViewSettingsDialog.open();
        });
    },

    handleFilterDialogConfirm: function (oEvent) {
      var oTable = this.getView().byId("idTableData"),
        mParams = oEvent.getParameters(),
        oBinding = oTable.getBinding("items"),
        aFilters = [];

      mParams.filterItems.forEach(function (oItem) {
        var sPath = oItem.getParent().getKey(),

          sOperator = sap.ui.model.FilterOperator.EQ,
          sValue1 = oItem.getKey(),
          oFilter = new Filter(sPath, sOperator, sValue1, sValue2);
        aFilters.push(oFilter);
      });

    },

    onDataExport: function (oEvent) {

      var aData = this.fnGetDataModel().getProperty("/HoursData");
      var oSettings;
      var oSheet;

      var aColumns = Default.fndownloadData();

      oSettings = {
        workbook: {
          columns: aColumns
        },
        dataSource: aData
      };

      oSheet = new sap.ui.export.Spreadsheet(oSettings);
      oSheet.build()
        .then(function () {
          MessageBox.show("Spreadsheet export has finished");
        })
        .finally(function () {
          oSheet.destroy();
        });
    },
		onCollapseAll: function() {
			var oTreeTable = this.byId("HourTreeTable");
			oTreeTable.collapseAll();
		},

		onCollapseSelection: function() {
			var oTreeTable = this.byId("HourTreeTable");
			oTreeTable.collapse(oTreeTable.getSelectedIndices());
		},

		onExpandFirstLevel: function() {
			var oTreeTable = this.byId("HourTreeTable");
			oTreeTable.expandToLevel(1);
		},

		onExpandSelection: function() {
			var oTreeTable = this.byId("HourTreeTable");
			oTreeTable.expand(oTreeTable.getSelectedIndices());
		}    

  });

});