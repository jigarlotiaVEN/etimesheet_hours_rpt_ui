sap.ui.define(["sap/ui/model/json/JSONModel", "sap/ui/Device"], function (JSONModel, Device) {
  "use strict";

  return {
    getChartData: function () {
      var chartData = {
        "categories": [
          {
            "exp": "", "name": "Dresses", "categories": [
              { "exp": "","name": "Casual Red Dress", "amount": 16.99, "currency": "EUR", "size": "S" },
              { "exp": "","name": "Short Black Dress", "amount": 47.99, "currency": "EUR", "size": "M" },
              { "exp": "","name": "Long Blue Dinner Dress", "amount": 103.99, "currency": "USD", "size": "L" }
            ]
          },
          {
            "exp": "","name": "Dresses", "categories": [
              { "exp": "","name": "Casual Red Dress", "amount": 16.99, "currency": "EUR", "size": "S" },
              { "exp": "","name": "Short Black Dress", "amount": 47.99, "currency": "EUR", "size": "M" },
              { "exp": "","name": "Long Blue Dinner Dress", "amount": 103.99, "currency": "USD", "size": "L" }
            ]
          }]

      };
      return chartData;
    },
    fnsetupData: function () {
      var Object = {};
      var setupData = {};
      var sStatus = [];
      Object.key = 1;
      Object.Status = "Submitted";
      Object.Value = "S";
      sStatus.push(Object);
      Object = {};

      Object.key = 2;
      Object.Status = "Draft";
      Object.Value = "D";
      sStatus.push(Object);
      Object = {};

      Object.key = 3;
      Object.Status = "Approved";
      Object.Value = "A";
      sStatus.push(Object);
      Object = {};

      Object.key = 4;
      Object.Status = "Rejected";
      Object.Value = "R";
      sStatus.push(Object);
      Object = {};


      var sPayCode = [];
      Object.key = 1;
      Object.Status = "Attendance";
      Object.Value = "ATT";
      sPayCode.push(Object);
      Object = {};

      Object.key = 2;
      Object.Status = "Absence";
      Object.Value = "ABS";
      sPayCode.push(Object);
      Object = {};

      setupData.Status = sStatus;
      setupData.PayCode = sPayCode;
      return setupData;

    },
    fndownloadData: function () {
      var Object = {};
      var downloadData = [];
      Object.label = "Employee Name";
      Object.property = "empl_FullName";
      downloadData.push(Object);
      Object = {};
      Object.label = "Employee ID";
      Object.property = "PERNR";
      downloadData.push(Object);
      Object = {};
      Object.label = "Status";
      Object.property = "time_Slice_Status";
      downloadData.push(Object);
      Object = {};
      Object.label = "Pay Codes";
      Object.property = "attabs_type";
      downloadData.push(Object);
      Object = {};
      Object.label = "Date";
      Object.property = "TIMESHEET_DATE";
      downloadData.push(Object);
      Object = {};
      Object.label = "Time In";
      Object.property = "TIMEIN";
      downloadData.push(Object);
      Object = {};
      Object.label = "TIMEOUT";
      Object.property = "Time Out";
      downloadData.push(Object);
      Object = {};
      Object.label = "Actual Hours";
      Object.property = "hours_Rec";
      downloadData.push(Object);
      return downloadData;
    },
  };
});