sap.ui.define(["sap/ui/test/Opa5"], function (Opa5) {
    "use strict";

    return Opa5.extend("ventia.polaris.etimesheethours.test.integration.arrangements.Startup", {
        iStartMyApp: function () {
            this.iStartMyUIComponent({
                componentConfig: {
                    name: "ventia.polaris.etimesheethours",
                    async: true,
                    manifest: true
                }
            });
        }
    });
});
