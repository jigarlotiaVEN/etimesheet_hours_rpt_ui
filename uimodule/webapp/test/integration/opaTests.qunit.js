/* global QUnit */

QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
    "use strict";

    sap.ui.require(["ventia/polaris/etimesheethours/test/integration/AllJourneys"], function () {
        QUnit.start();
    });
});
